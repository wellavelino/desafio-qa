# language: pt

Funcionalidade: Visualização de informações de usuários 

	Eu quero ver as informações dos meus contatos usando o atalho tab "conversas"
	Para isso preciso estar com o registro ativo e ter alguma conversa ativa


	Cenário: Ampliar foto do contato

		Dado que estou na tab "Conversas" 
		Quando eu clicar na foto do contato
		Então vejo a foto do contato
		E outras opções de iteração 		
		|Opção 1			 |Opção 2			 |Opção 3	         	 |
		|Atalho para conversa|Atalho para ligação|Atalho para informações|


	Cenário: Realizar ligações pelo atalho de contato

		Dado que estou na tab "Conversas"
		Quando eu clicar na foto do contato
		Então clico para realizar uma ligação
		Mas não estou conectado a internet
		Então eu vejo a mensagem "Não foi possível completar a ligação. Tenha certeza que seu telefone está conectado com a internet e tente novamente"


	Cenário: Visualizar perfil do meu contato pelo atalho

		Dado que estou na tab "Conversas"
		Quando eu clicar na foto do contato
		Então clico para visualizar o perfil do meu contato
		E vejo o detalhamento de todas as informações
		| Status | Midia | Grupos com contato | Telefone   |
		| Desafio|  QA   | Concrete Solutions | 9-8877-xxxx|


