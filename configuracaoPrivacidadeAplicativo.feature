# language: pt

	Funcionalidade: Configuração de privacidade do aplicativo
		
	 
	Eu quero configurar o acesso as informações privadas do aplicativo
	Para garantir que apenas conhecidos tenham acesso a essas informações


 	Cenário: Atualização de visto por último nas conversas
 	
 		Dado que estou na tela de configuração de privacidade
 		Quando eu clicar sobre visto por último 
 		Então eu vejo a lista dos níveis de privacidade		
 		|Todos         |
 		|Meus contatos | 
 		|Ninguém       | 
 		|Cancelar      | 		
 		E quando selecionar atualizar com a nova privacidade 		
 		
 	Cenário: Sem rede disponível
 			
 		Dado que estou na tela de configuração de privacidade
 		Quando eu clicar sobre visto por último
 		Então eu vejo a lista dos níveis de privacidade		
 		|Todos         |
 		|Meus contatos | 
 		|Ninguém       | 
 		|Cancelar      |
 		E quando eu selecionar atualizar com a nova privacidade
 		Mas se não existir rede disponível eu vejo a seguinte mensagem "Rede indisponível.Por favor,tente novamente mais tarde."
 		
 	Cenário: Acionar botão home do celular
 	
 		Dado que estou na tela de configuração de privacidade
 		Quando eu clicar sobre visto por último
 		Então eu vejo a lista dos níveis de privacidade		
 		|Todos         |
 		|Meus contatos | 
 		|Ninguém       | 
 		|Cancelar      |
 		Mas eu aciono botão home do celular 
 		E quando voltar para o aplicativo eu vejo a lista dos níveis de privacidade 		
 		
 	Cenário: Alteração de idioma do celular
 		
 		Dado que estou na tela de configuração de privacidade
 		Quando eu clicar sobre visto por último
 		Então eu vejo a lista dos níveis de privacidade		
 		|Todos         |
 		|Meus contatos | 
 		|Ninguém       | 
 		|Cancelar      |
 		Mas altero o idioma do celular 
 		E o aplicativo muda o idioma para o idioma escolhido